package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class SignupActivity extends AppCompatActivity {

    public static final String APP_ID ="0A6F84C6-158F-2890-FFCD-A76BD9A11600";
    public static final String SECRET_KEY ="A266CEE9-19CE-6CE0-FFB8-1B4E77B01D00";
    public static final String VERSION ="v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        SignupFragment signup = new SignupFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.signupcontainer, signup).commit();

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
    }
}
