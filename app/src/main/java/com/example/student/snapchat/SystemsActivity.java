package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SystemsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_systems);

        SystemsFragment systems = new SystemsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, systems).commit();

    }
}
