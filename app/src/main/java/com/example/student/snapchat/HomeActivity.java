package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        HomeFragment home = new HomeFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, home).commit();
    }
}
